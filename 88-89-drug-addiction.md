```
Drug addiction, as noted by neurologists and pharmacologists, is
a kind of “hijacking” of the brain that subconsciously leads a person’s
behavior toward the reward of use. The addict is often fully disgusted

by his (or her) actions, but the addiction pattern continues as changes to
brain chemistry drive changes to both thoughts and behavior, justify-
ing the next use of a drug. One may indeed overcome an addiction by
choice, willpower, and likely external help, but that single end does not
discount the general loss of control found during the addiction period.
Likewise, analysis that takes a biosocial view of the issue finds great
evidence that child abuse, stress, and emotional loss can contribute to
one’s development of addiction as a form of self-medication. In fact,
an entire range of health and behavioral issues can arise later in life,
since childhood abuse literally changes and damages the brain. Not only
can abuse disturb the brain’s neurochemistry, but it can also shrink and
distort critical areas, such as the hippocampus. This can lead to severe
impairments in one’s ability to make decisions and judgments, in the
same way a person who has severe brain damage from head trauma in
a car accident may no longer be able to speak. Do we judge a person’s
speech impairment in such a case as a problem of free will? No, we
understand the impairment.
```
